<?php
class HourlyProject extends Project {

    protected $totalHours;
    protected $spentHours;
    protected $pricePerHour;

    public function __construct($projectName, $description, $totalHours, $spentHours, $pricePerHour) {
        parent::__construct($projectName, $description);
        $this->totalHours = $totalHours;
        $this->spentHours = $spentHours;
        $this->pricePerHour = $pricePerHour;
    }

    public function getPrice() {
        return $this->totalHours * $this->pricePerHour;
    }

    public function getProjectProgress() {
        return round($this->spentHours * 100 / $this->totalHours, 0);
    }

    public function getTitle() {
        return $this->projectName;
    }

    public function getDescription() {
        return $this->description;
    }

}