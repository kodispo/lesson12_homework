<?php
class MilestoneProject extends Project {

    protected $totalMilestones;
    protected $currentMilestone;
    protected $projectPrice;

    public function __construct($projectName, $description, $totalMilestones, $currentMilestone, $projectPrice) {
        parent::__construct($projectName, $description);
        $this->totalMilestones = $totalMilestones;
        $this->currentMilestone = $currentMilestone;
        $this->projectPrice = $projectPrice;
    }

    public function getPrice() {
        return $this->projectPrice;
    }

    public function getProjectProgress() {
        return round($this->currentMilestone * 100 / $this->totalMilestones, 0);
    }

    public function getTitle() {
        return $this->projectName;
    }

    public function getDescription() {
        return $this->description;
    }
}