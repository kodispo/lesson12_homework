<?php
class ProjectFeedWriter {

    protected $projects = [];

    public function addProject(Workable $project){
        $this->projects[] = $project;
    }

    public function writeAll() {
        $html = '';
        foreach ($this->projects as $project) {
            $html .= $this->writeProject($project);
        }
        return $html;
    }

    protected function writeProject(Workable $project) {
        $html = '';
        ob_start(); ?>

        <ul class="list-group list-group-flush mb-5">
            <li class="list-group-item">
                <strong class="text-uppercase text-primary"><?= $project->getTitle(); ?></strong>
                <span class="badge badge-info text-uppercase p1"><?= get_class($project); ?></span>
            </li>
            <li class="list-group-item"><strong>Description: </strong><?= $project->getDescription(); ?></li>
            <li class="list-group-item"><strong>Price: </strong>$<?= $project->getPrice(); ?></li>
            <?php if ($project instanceof Project) : ?>
                <li class="list-group-item">
                    <strong>Progress: </strong>
                    <div class="progress mt-1">
                        <div class="progress-bar"
                             role="progressbar"
                             style="width: <?= $project->getProjectProgress(); ?>%;"
                             aria-valuenow="<?= $project->getProjectProgress(); ?>"
                             aria-valuemin="0"
                             aria-valuemax="100">
                            <?= $project->getProjectProgress(); ?>%
                        </div>
                    </div>
                </li>
            <?php endif; ?>
        </ul>

        <?php $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }

}