<?php
abstract class Project implements Workable {

    protected $projectName;
    protected $description;

    public function __construct($projectName, $description) {
        $this->projectName = $projectName;
        $this->description = $description;
    }

    abstract public function getPrice();

    abstract public function getProjectProgress();

}