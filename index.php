<?php
ini_set('display_errors', 'On');

require_once 'interfaces/Workable.php';
require_once 'classes/Project.php';
require_once 'classes/HourlyProject.php';
require_once 'classes/MilestoneProject.php';
require_once 'classes/Task.php';
require_once 'classes/ProjectFeedWriter.php';

$hourlyProjects = [];
$hourlyProjects[] = new HourlyProject('Digitalmech', 'Integrate python-written API calls', 80, 32, 35);
$hourlyProjects[] = new HourlyProject('Vollara Stream', 'Developer needed to update Laravel Web App', 16, 4, 23);

$milestoneProjects = [];
$milestoneProjects[] = new MilestoneProject('Zigmund Pro', 'Video game community builder', 6, 3, 800);
$milestoneProjects[] = new MilestoneProject('Gogo', 'Drupal, Wordpress, and Shopify', 13, 9, 1250);

$tasks = [];
$tasks[] = new Task('POS Configuration', 'Create configuration files for installing Odoo via docker', 500);
$tasks[] = new Task('Mobile Payment Integration', 'Task is to integrate mobile payment', 170);

$writer = new ProjectFeedWriter();

foreach ($hourlyProjects as $hourlyProject) $writer->addProject($hourlyProject);
foreach ($milestoneProjects as $milestoneProject) $writer->addProject($milestoneProject);
foreach ($tasks as $task) $writer->addProject($task);


/*
 * html output
 */
require_once 'parts/header.php';
echo $writer->writeAll();
require_once 'parts/footer.php';